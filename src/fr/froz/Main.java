package fr.froz;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.logging.Logger;

/*All right reserved to Froz
* tous droits reservé a Froz
* Interdiction d'utlisation en dehors de l'accord
* explicite de Théo Matis */
public class Main extends JavaPlugin implements Listener {
    static Boolean[] freeArena = {true, true, true, true, true};
	private ArrayList<Player> qPlayer = new ArrayList<>();
    private Logger log = Bukkit.getLogger();

    static void unRegisterE(Listener l) {
        HandlerList.unregisterAll(l);
    }

    @Override
    public void onEnable() {
        log.info("[WallsBattle] plugin by Froz all rights reserved");
        Bukkit.getPluginManager().registerEvents(this, this);
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    }

    @Override
    public void onDisable() {
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("gameAdmin") && sender.hasPermission("AriaCraft.modo")) {
            Player p = (Player) sender;
            qPlayer.remove(p);
            p.setGameMode(GameMode.SPECTATOR);
            p.sendMessage(ChatColor.RED + "Vous etes en mode modo");
            return true;
        }
        return false;
    }
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        e.setJoinMessage("");
        qPlayer.add(e.getPlayer());
        e.getPlayer().setGameMode(GameMode.SPECTATOR);
        e.getPlayer().setInvulnerable(false);
        e.getPlayer().setWalkSpeed(0.2F);
        e.getPlayer().teleport(new Location(Bukkit.getWorld("world"), 465, 8, 506));
        for (PotionEffect effect : e.getPlayer().getActivePotionEffects())
            e.getPlayer().removePotionEffect(effect.getType());
        e.getPlayer().sendMessage(ChatColor.DARK_GRAY + " -------------" + ChatColor.DARK_AQUA + "[" + ChatColor.RED + "Walls" + ChatColor.DARK_RED + "" + ChatColor.BOLD + "Battle" + ChatColor.RESET + ChatColor.DARK_AQUA + "]" + ChatColor.DARK_GRAY + "-------------\n \n" + ChatColor.WHITE + "    Bienvenue sur le WallsBattle d'" + ChatColor.DARK_BLUE + "Aria" + ChatColor.DARK_AQUA + "Craft \n \n" + ChatColor.DARK_GRAY + " --------------------------------------");
        if (qPlayer.size() == 2) {
            int inte = 0;
            boolean run = true;
            while (freeArena.length > inte && run) {
                if (freeArena[inte]) {
                    freeArena[inte] = false;
                    Arena a = new Arena(new Location(Bukkit.getWorld("world"), 500 * inte, 0, 0), inte);
                    a.addPlayer(qPlayer.get(0));
                    a.addPlayer(qPlayer.get(1));
                    qPlayer = new ArrayList<>();
                    run = false;
                    a.start();
                }
                inte = inte + 1;


            }

        }
    }
}
